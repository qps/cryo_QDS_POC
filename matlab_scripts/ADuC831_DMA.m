%% ******************* CHANNEL 0 *********************

%% Input Voltage Range Measurements
%{
[f100Hz_2_5V, fig_f100Hz_2_5V] = sig_gen_input('key',100,2.5,0,'dma');
%saveas(fig_f100Hz_2_5V,'aduc831\dma_mode\room_temp\ch0_fig2\f100Hz_2_5V.fig')
%save('aduc831\dma_mode\room_temp\ch0_data2\f100Hz_2_5V.mat', 'f100Hz_2_5V')
saveas(fig_f100Hz_2_5V,'aduc831\dma_mode\cryo\ch0_fig2\f100Hz_2_5V.fig')
save('aduc831\dma_mode\cryo\ch0_data2\f100Hz_2_5V.mat', 'f100Hz_2_5V')

[f100Hz_1_25V, fig_f100Hz_1_25V] = sig_gen_input('key',100,1.25,0,'dma');
%saveas(fig_f100Hz_1_25V,'aduc831\dma_mode\room_temp\ch0_fig2\f100Hz_1_25V.fig')
%save('aduc831\dma_mode\room_temp\ch0_data2\f100Hz_1_25V.mat', 'f100Hz_1_25V')
saveas(fig_f100Hz_1_25V,'aduc831\dma_mode\cryo\ch0_fig2\f100Hz_1_25V.fig')
save('aduc831\dma_mode\cryo\ch0_data2\f100Hz_1_25V.mat', 'f100Hz_1_25V')

[f100Hz_0_25V, fig_f100Hz_0_25V] = sig_gen_input('key',100,0.25,0,'dma');
%saveas(fig_f100Hz_0_25V,'aduc831\dma_mode\room_temp\ch0_fig2\f100Hz_0_25V.fig')
%save('aduc831\dma_mode\room_temp\ch0_data2\f100Hz_0_25V.mat', 'f100Hz_0_25V')
saveas(fig_f100Hz_0_25V,'aduc831\dma_mode\cryo\ch0_fig2\f100Hz_0_25V.fig')
save('aduc831\dma_mode\cryo\ch0_data2\f100Hz_0_25V.mat', 'f100Hz_0_25V')


%% Bandwidth Measurements
bd_data = cell(1,9);
[f100Hz_1V, ~] = sig_gen_input('key',100,1,0,'dma');
bd_data{1,1} = f100Hz_1V;
for i = 1:8
    [data, fig] = sig_gen_input('key',i*1000,1,0,'dma');
    bd_data{1,i+1} = data;
end
%save('aduc831\dma_mode\room_temp\ch0_data2\bd_data.mat','bd_data')
save('aduc831\dma_mode\cryo\ch0_data\bd_data2.mat','bd_data')

%% Dynamic Performance
f1kHz_0_25V = sig_gen_input('key',1000,0.25,0,'dma');
%save('aduc831\dma_mode\room_temp\ch0_data2\f1kHz_0_25V.mat', 'f1kHz_0_25V')
save('aduc831\dma_mode\cryo\ch0_data2\f1kHz_0_25V.mat', 'f1kHz_0_25V')

f1kHz_2_25V = sig_gen_input('key',1000,2.25,0,'dma');
%save('aduc831\dma_mode\room_temp\ch0_data2\f1kHz_2_25V.mat', 'f1kHz_2_25V')
save('aduc831\dma_mode\cryo\ch0_data2\f1kHz_2_25V.mat', 'f1kHz_2_25V')


%% CHANGE INPUT TO DIGISTANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to Digistant. Continue? [y+Enter]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data = linearity_mes(0.05,2.45,49,831,0);
end
%save('aduc831\dma_mode\room_temp\ch0_data2\dig_data.mat','dig_data')
save('aduc831\dma_mode\cryo\ch0_data2\dig_data.mat','dig_data')

%% CHANGE INPUT TO BATTERY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to 3V battery. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    data = serial_readout_dma(0);
    pause(5);
    %Process and plot the data
    [~,~,~, bat_1_5V,fig_bat1_5V] = data_processing(data,831);
    %saveas(fig_bat1_5V,'aduc831\dma_mode\room_temp\ch0_fig2\fig_bat1_5V.fig')
    %save('aduc831\dma_mode\room_temp\ch0_data2\bat_1_5V.mat', 'bat_1_5V')
    saveas(fig_bat1_5V,'aduc831\dma_mode\cryo\ch0_fig2\fig_bat1_5V.fig')
    save('aduc831\dma_mode\cryo\ch0_data2\bat_1_5V.mat', 'bat_1_5V')
end

%% CHANGE INPUT TO GND
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be grounded. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    data = serial_readout_dma(0);
    pause(5);
    %Process and plot the data
    [~,~,~, in_gnd,fig_in_gnd] = data_processing(data,831);
    %saveas(fig_in_gnd,'aduc831\dma_mode\room_temp\ch0_fig2\fig_in_gnd.fig')
    %save('aduc831\dma_mode\room_temp\ch0_data2\in_gnd.mat', 'in_gnd')
    saveas(fig_in_gnd,'aduc831\dma_mode\cryo\ch0_fig2\fig_in_gnd.fig')
    save('aduc831\dma_mode\cryo\ch0_data2\in_gnd.mat', 'in_gnd')
end

%% ******************* CHANNEL 1 *********************

prompt = 'Input should be on channel 1. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
   str
end
%% Input Voltage Range Measurements

[f100Hz_2_5V, fig_f100Hz_2_5V] = sig_gen_input('key',100,2.5,1,'dma');
%saveas(fig_f100Hz_2_5V,'aduc831\dma_mode\room_temp\ch1_fig2\f100Hz_2_5V.fig')
%save('aduc831\dma_mode\room_temp\ch1_data2\f100Hz_2_5V.mat', 'f100Hz_2_5V')
saveas(fig_f100Hz_2_5V,'aduc831\dma_mode\cryo\ch1_fig2\f100Hz_2_5V.fig')
save('aduc831\dma_mode\cryo\ch1_data2\f100Hz_2_5V.mat', 'f100Hz_2_5V')

[f100Hz_1_25V, fig_f100Hz_1_25V] = sig_gen_input('key',100,1.25,1,'dma');
%saveas(fig_f100Hz_1_25V,'aduc831\dma_mode\room_temp\ch1_fig2\f100Hz_1_25V.fig')
%save('aduc831\dma_mode\room_temp\ch1_data2\f100Hz_1_25V.mat', 'f100Hz_1_25V')
saveas(fig_f100Hz_1_25V,'aduc831\dma_mode\cryo\ch1_fig2\f100Hz_1_25V.fig')
save('aduc831\dma_mode\cryo\ch1_data2\f100Hz_1_25V.mat', 'f100Hz_1_25V')

[f100Hz_0_25V, fig_f100Hz_0_25V] = sig_gen_input('key',100,0.25,1,'dma');
%saveas(fig_f100Hz_0_25V,'aduc831\dma_mode\room_temp\ch1_fig2\f100Hz_0_25V.fig')
%save('aduc831\dma_mode\room_temp\ch1_data2\f100Hz_0_25V.mat', 'f100Hz_0_25V')
saveas(fig_f100Hz_0_25V,'aduc831\dma_mode\cryo\ch1_fig2\f100Hz_0_25V.fig')
save('aduc831\dma_mode\cryo\ch1_data2\f100Hz_0_25V.mat', 'f100Hz_0_25V')

%% Bandwidth Measurements
bd_data = cell(1,9);
[f100Hz_1V, ~] = sig_gen_input('key',100,1,1,'dma');
bd_data{1,1} = f100Hz_1V;
for i = 1:8
    [data, fig] = sig_gen_input('key',i*1000,1,1,'dma');
    bd_data{1,i+1} = data;
end
%save('aduc831\dma_mode\room_temp\ch1_data2\bd_data.mat','bd_data')
save('aduc831\dma_mode\cryo\ch1_data2\bd_data.mat','bd_data')

%% Dynamic Performance
f1kHz_0_25V = sig_gen_input('key',1000,0.25,1,'dma');
%save('aduc831\dma_mode\room_temp\ch1_data2\f1kHz_0_25V.mat', 'f1kHz_0_25V')
save('aduc831\dma_mode\cryo\ch1_data2\f1kHz_0_25V.mat', 'f1kHz_0_25V')

f1kHz_2_25V = sig_gen_input('key',1000,2.25,1,'dma');
%save('aduc831\dma_mode\room_temp\ch1_data2\f1kHz_2_25V.mat', 'f1kHz_2_25V')
save('aduc831\dma_mode\cryo\ch1_data2\f1kHz_2_25V.mat', 'f1kHz_2_25V')


%% CHANGE INPUT TO DIGISTANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to Digistant. Continue? [y+Enter]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data = linearity_mes(0.05,2.45,49,831,1);
end
%save('aduc831\dma_mode\room_temp\ch1_data2\dig_data.mat','dig_data')
save('aduc831\dma_mode\cryo\ch1_data2\dig_data.mat','dig_data')

%% CHANGE INPUT TO BATTERY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to 1.5V battery. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    data = serial_readout_dma(1);
    pause(5);
    %Process and plot the data
    [~,~,~, bat_1_5V,fig_bat1_5V] = data_processing(data,831);
    %saveas(fig_bat1_5V,'aduc831\dma_mode\room_temp\ch1_fig2\fig_bat1_5V.fig')
    %save('aduc831\dma_mode\room_temp\ch1_data2\bat_1_5V.mat', 'bat_1_5V')
    saveas(fig_bat1_5V,'aduc831\dma_mode\cryo\ch1_fig2\fig_bat1_5V.fig')
    save('aduc831\dma_mode\cryo\ch1_data2\bat_1_5V.mat', 'bat_1_5V')
end

%% CHANGE INPUT TO GND
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be grounded. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    data = serial_readout_dma(1);
    pause(5);
    %Process and plot the data
    [~,~,~, in_gnd,fig_in_gnd] = data_processing(data,831);
    %saveas(fig_in_gnd,'aduc831\dma_mode\room_temp\ch1_fig2\fig_in_gnd.fig')
    %save('aduc831\dma_mode\room_temp\ch1_data2\in_gnd.mat', 'in_gnd')
    saveas(fig_in_gnd,'aduc831\dma_mode\cryo\ch1_fig2\fig_in_gnd.fig')
    save('aduc831\dma_mode\cryo\ch1_data2\in_gnd.mat', 'in_gnd')
end
%}

