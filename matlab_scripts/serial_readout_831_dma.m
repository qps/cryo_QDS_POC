 function [data] = serial_readout_dma(chan)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  INPUTS :
    %%chan = ADC channel {0,1}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  OUTPUTS :
    %%data = data acquired from the uController (xdata) in DMA mode
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% OBJECT INITIALIZATION
    %Create uController obj 
    uC = serial('com4','BaudRate',9600,'DataBits',8,'Parity','none');
    uC.InputBufferSize=7000;
    fopen(uC);
    
    %% DATA ACQUISITION
    fprintf('Channel selected : %d\n', chan) 
    fwrite(uC,sprintf('%d',chan)); %uController is waiting for the channel to be selected

    mes = fscanf(uC); %uController is waiting for 's' to be pressed in order to start conversions
    fprintf('message : %s\n',mes) 
    fwrite(uC,'s','char'); %Print 's' to serial
    fprintf('Starting Conversions...\n')
    data = fread(uC); %Acquire the data from the uController
    data = data(1:5116);
    fclose(uC);
    end

