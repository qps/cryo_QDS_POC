function [s,x,y,norm]=dynamic_performance(data,offset,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  INPUTS :
%%data = signal data to be processed
%%offset = offset of the signal
%%mode = norm or dma for ADuC831 , 834 for ADuC834
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% FFT
data = data-offset;
%ADuC831 : DMA or Normal Mode
if strcmp(mode,'norm')
    fs= 12500;
    L = 1000;
elseif strcmp(mode,'dma')
    fs = 38400;
    L = 1023;
%ADuC834
elseif strcmp(mode,'834')
    fs = 16.65;
    L = 682;
end
f = fs*(1:((L+12-1)/2-1))/(L+12-1);

%Producing single sided fft
%w = kaiser(48,38);
w = hamming(12);
%w = blackmanharris(64);
s = conv(data,w);
Y = fft(s);
P2 = abs(Y/(L+12-1));
P1 = P2(1:(L+12-1)/2-1);
P1(2:end-1) = 2*P1(2:end-1);
norm = max(P1); %normalization value

%% THD
thd(data,fs);

%% PLOTTING FFT
x = f;
y = 20*log10(P1/4);
figure;
plot(x,y,'Color','y')
xlabel('Frequency (Hz)');
ylabel('db');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
ax.GridAlpha = 0.3;
end