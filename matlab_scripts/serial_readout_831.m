    function [uC,data] = serial_readout(chan,comport,baud_rate,input_buffer)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  INPUTS :
    %%chan = ADC channel {0,1}
    %%comport = serial port {1,2,3,4,..}
    %%baud_rate = baudrate {9600,..}
    %%input_buffer = data bytes acquired {..,1000}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  OUTPUTS :
    %%uC = uController obj
    %%data = data acquired from the uController
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% OBJECT INITIALIZATION
    %Create uController obj from serial port specified in the input
    uC = serial(sprintf('com%d',comport),'BaudRate',baud_rate,'DataBits',8,'Parity','none');
    %Each line from the serial contains 5 ASCII characters, 3 of which
    %contain data information needed and the other two are /r/n. In order
    %to obtain 1000 of 12-bit values, we need 5*1000 ASCII characters.
    uC.InputBufferSize=input_buffer*5;
    fopen(uC);
    
    %% DATA ACQUISITION
    fprintf('Channel selected : %d\n', chan) 
    fwrite(uC,sprintf('%d',chan)); %uController is waiting for the channel to be selected

    mes = fscanf(uC); %uController is waiting for 's' to be pressed in order to start conversions
    fprintf('message : %s\n',mes) 
    fwrite(uC,'s','char'); %Print 's' to serial
    fprintf('Starting Conversions...\n')
    data = fread(uC,input_buffer*5); %Acquire the data from the uController

    fclose(uC);
    end

