function dig_data = linearity_mes(v1,v2,N,aduc,ch,range)

amp = linspace(v1,v2,N);
sot = char(2);
eot = char(3);
lf = char(10);
dig_data = cell(3,N);
j = 1;


dig = visa('ni','ASRL1::INSTR');
dig.Timeout =10;
fopen(dig);

keith = gpib('ni',0,16);
fopen(keith);   

for i = drange(amp)
    
    fprintf(dig,sot);
    fprintf(dig,sprintf('SOUR:VOLT %0.3f',i));
    fprintf(dig,lf);
    fprintf(dig,eot);
    res = fscanf(dig);
    if double(res(2))==6
        fprintf(keith,'MEAS:VOLT:DC?');
        dig_data{3,j} = fscanf(keith);
        if aduc==831
            data = serial_readout_dma(ch);
            [~,~,~,data_norm,~] = data_processing(data,831);
        elseif aduc==834
            [~,data] = serial_readout_834(5,115200,682,range);
            if strcmp(range,'a')
                lsb = 20;
            elseif strcmp(range,'b')
                lsb =2.56;
            end
            [~,~,~,data_norm,~] = data_processing(data,834,lsb,1);
        end
        dig_data{1,j} = data_norm;
        dig_data{2,j} = i;
        
        j = j+1;
    else 
        printf('ERROR')
    end
end

fclose(dig);
fclose(keith);        



   