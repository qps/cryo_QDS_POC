%addpath('normal_mode/ch1_data/linearity');
%addpath('normal_mode/ch1_data/linearity2');
%addpath('normal_mode/ch0_data/linearity');

m = [];in = [];
%{
for i = 0.1:0.1:2.4
    data = importdata(sprintf('dig_%0.1fV_ch0.mat',i));
    m = [m (mean(data)-i)];
end 
%}

%data = importdata('dig_0.05_step_ch1.mat');
data = dig_data;

for i = 1:length(data)
    me = mean(data{1,i});
    
    if (data{3,i}(13)=='N')
        str = data{3,i}(2:12);
    else
        str = data{3,i}(2:13);
    end
    mes_in = str2double(str);
    in = [in mes_in];

    %mes_in = (i-1)*10^(-3);
    %in = [in mes_in];
    m = [m abs(me-mes_in)];
end
    

y = m;
x = in;

figure;
p = plot(x,y);
p.Color = 'y';
p.Marker = '*';
p.MarkerEdgeColor = 'c';
xlabel('Input Voltage (V)');
ylabel('|Output-Input Voltage| (mV)');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
ax.GridAlpha = 0.5;


