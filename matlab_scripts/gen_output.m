
function gen_output(obj,func,freq,amp,l_volt,h_volt)



%%General Output Configurations
fprintf(obj,sprintf('FUNC %s',func)); %Select the output function
fprintf(obj,'OUTP:LOAD TERM'); %Define the instruments output load as 50ohm
fprintf(obj,'OUTP:OFF 0'); %Activate/Disactivate the output offset voltage

%%Sine Wave Configuration
fprintf(obj, sprintf('FREQ %g',freq)); %Define the output frequency
fprintf(obj,'VOLT VOLT'); %Set the Amplitude Unit
fprintf(obj,sprintf('VOLT %0.2g',amp)); %Select the Amplitude Voltage Value
fprintf(obj,sprintf('VOLT:HIGH %0.2g',l_volt)); %Define the High Level Voltage
fprintf(obj,sprintf('VOLT:LOW %0.2g',h_volt)); %Define the Low Level Voltage

fprintf(obj,'OUTP 1') %Activate the instrument output
ison = query(obj,'OUTP?')

fprintf('Output ON : %s Wave with %dHz Frequency & %dV Amplitude', func,freq,amp);
end