function [uC,data] = serial_readout_834(comport,baud_rate,input_buffer,range)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  INPUTS :
    %%chan = ADC channel {0,1}
    %%comport = serial port {1,2,3,4,..}
    %%baud_rate = baudrate {9600,..}
    %%input_buffer = data bytes acquired {..,1000}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%  OUTPUTS :
    %%uC = uController obj
    %%data = data acquired from the uController
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% OBJECT INITIALIZATION
    %Create uController obj from serial port specified in the input
    uC = serial(sprintf('com%d',comport),'BaudRate',baud_rate,'DataBits',8,'Parity','none');
    %Each line from the serial contains 5 ASCII characters, 3 of which
    %contain data information needed and the other two are /r/n. In order
    %to obtain 1000 of 12-bit values, we need 5*1000 ASCII characters.
    uC.InputBufferSize=input_buffer*8 + 118;
    fopen(uC);
    
    %% DATA ACQUISITION
    fwrite(uC,sprintf('%s',range),'char'); %'a' for 0-20mV and 'b' for 0-2.56V
    pause(0.1);
    fwrite(uC,'x','char'); %'x' for xdata buffer, 's' for data streaming
    fprintf('Starting Conversions...\n')
    pause(50);
    data = fread(uC); %Acquire the data from the uController
    data = data(119:end);
    fclose(uC);
    end

