function gen = Create_gen(timeout, comport)
%Create visa instrument for R&S HMF2550 Arbitrary Generator 
   
    gen = visa('ni',sprintf('ASRL%d::INSTR',comport)); %Serial connection : ASRL6=COM6
    gen.Timeout = timeout; %The maximum time (sec) to wait to complete a read or write operation
   
    fopen(gen); 
    
    fprintf(gen,'*IDN?'); %Asking for the identification string of the instrument
    idn_gen=fscanf(gen);
    fprintf('\nFunction Generator Identification string: %s\n', idn_gen)
end