function [p_db,db,pks,ind,yy] =  bandwidth_fft(all_data,f1,f2,offset)

pa = []; db = [];
y = cell(1,length(all_data));
p = cell(1,length(all_data));
p_max = [];
fs = 16.65;
L = 682;
%L = 1023;
%fs = 38400;
%fs= 153600;
%L = 1000;
f = fs*(0:(L/2))/L;
figure(1);
for i = 1:length(all_data)
    y{i} = fft(all_data{i}-offset);
    P2 = abs(y{i}/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    p{i}=P1;
    p_max = [p_max max(P1)];
    %p_db{i} = 20*log10(P1);
    %db = [db p_db{i}];
    subplot(2,1,1)
    plot(f,P1);
    xlabel('Frequency (Hz)');
    ylabel('db full scale');
    hold on
    ax = gca;
    grid on 
    grid minor
    ax.GridAlpha = 0.3;
end

norm=max(p_max);
for i = 1:length(all_data)
p_db{i}=20*log10(p{i}/norm);
db = [db max(p_db{i})];
end
[pks,ind] = findpeaks(db);
%xx = sort([f1 ind*0.1  f2]);
xx = sort(ind*0.1);
yy = [];
for j = 1:length(xx)
    k = int8(xx(j)*10);
    yy = [yy db(k)];
end

figure(1);
subplot(2,1,2);
plot(linspace(f1,f2,length(all_data)),db);
xlabel('Frequency (Hz)');
ylabel('db full scale');
grid on
grid minor
 
hold on
%plot([f1 ind*0.1 f2],[db(1) pks db(end)]);
plot(xx, yy);


figure(2);
plot(linspace(f1,f2,length(all_data)),db,'y');
%plot(xx,yy,'Color','y');
xlabel('Frequency (Hz)');
ylabel('db full scale');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
ax.GridAlpha = 0.3;

end