function [final_data,fig] = sig_gen_input(fgen,freq,amp,chan,mode)

%%  INSTRUMENTS INITIALIZATION

%Create&Open generator with Inputs: timeout, comport
gen = Create_gen(fgen,10,6);  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HAMEG HMF2550
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(fgen,'ham') == 1
    %% INPUT CONFIGURATION
    fprintf(gen,'FUNC SIN'); %Select the output function
    fprintf(gen,'OUTP:LOAD 10000'); %Define the instruments output load as 50ohm
    fprintf(gen,'OUTP:OFFS 1'); %Activate/Disactivate the output offset voltage
    fprintf(gen, sprintf('FREQ %d',freq)); %Define the output frequency
    fprintf(gen,'VOLT:UNIT VOLT'); %Set the Amplitude Unit
    fprintf(gen, sprintf('VOLT %0.3f', amp)); %Select the Amplitude Voltage Value
    %fprintf(gen,'VOLT:HIGH 2.5'); %Define the High Level Voltage
    %fprintf(gen,'VOLT:LOW 1.25'); %Define the Low Level Voltage
    fprintf(gen, sprintf('VOLT:OFFS %0.3f',amp/2)); %Define the output offset value depending on
    %the amplitude setting
    fprintf(gen,'OUTP 1'); %Activate the instrument output
    pause(5);
    
    %% DATA ACQUISITION
    %Serial readout of the data. Creates uC object with Inputs:
    %channel,comport,baud_rate,input_buffer
    if strcmp(mode,'norm')
        [~,data] = serial_readout(chan,4,9600,1000); 
        L = 1000;
        fs = 12500;
    elseif strcmp(mode,'dma')
        [data] = serial_readout_dma(chan); 
        L = 1023;
        fs = 38400;
    end
    %Process and plot the data
    pause(5);
    [~,~,~, final_data,fig] = data_processing(data,831);
    fprintf(gen,'OUTP 0');
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEYSIGHT 33500B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(fgen,'key') == 1
    %% SINWAVE INPUT CONFIGURATION
    fprintf(gen,'OUTP1:LOAD INF'); %High Z
    fprintf(gen,'SOUR1:FUNC SIN'); %Select the output function on CH1
    fprintf(gen,sprintf('SOUR1:FREQ %0.1f',freq)); %Frequency from the input
    fprintf(gen,'SOUR1:VOLT:UNIT VPP'); %Amplitude unit Vpp
    fprintf(gen,sprintf('SOUR1:VOLT %0.3f', amp)); %Amplitude from the input
    fprintf(gen,sprintf('SOUR1:VOLT:OFFS %0.3f',(amp/2))); %Offset
    fprintf(gen, 'OUTP1 ON');
    
    pause(5);
    %% DATA ACQUISITION
    %Serial readout of the data. Creates uC object with Inputs:
    %channel,comport,baud_rate,input_buffer
    if strcmp(mode,'norm')
        [~,data] = serial_readout(chan,4,9600,1000); 
        L = 1000;
        fs = 12500;
    elseif strcmp(mode,'dma')
        [data] = serial_readout_dma(chan); 
        L = 1023;
        fs = 38400;
    end
    %Process and plot the data
    pause(5);
    [~,~,~, final_data,fig] = data_processing(data,831);
    fprintf(gen,'OUTP 0');
end
fclose(gen);
%{
%%
%%PLOTTING
figure;
x = L*(1:L)/fs;
%x = linspace(1,L,L);
y = final_data;
plot(x,y);
xlabel('Time (ms)');
ylabel('Amplitude (V)');
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
grid on 
grid minor
ax.GridAlpha = 0.3;
%}
end



