%% Input Voltage Range Measurements
%{
[f100mHz_2_56V_2_5off, fig_f100mHz_2_56V_2_5off] = sig_gen_input_834('key',0.1,2.56,2.5,2.56,'b');
%saveas(fig_f100mHz_2_56V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_2_56V_2_5off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_2_56V_2_5off.mat', 'f100mHz_2_56V_2_5off')
saveas(fig_f100mHz_2_56V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig\f100mHz_2_56V_2_5off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\f100mHz_2_56V_2_5off.mat', 'f100mHz_2_56V_2_5off')

[f100mHz_1_5V_2_5off, fig_f100mHz_1_5V_2_5off] = sig_gen_input_834('key',0.1,1.5,2.5,2.56,'b');

%saveas(fig_f100mHz_1_5V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_1_5V_2_5off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_1_5V_2_5off.mat', 'f100mHz_1_5V_2_5off')
saveas(fig_f100mHz_1_5V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig\f100mHz_1_5V_2_5off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\f100mHz_1_5V_2_5off.mat', 'f100mHz_1_5V_2_5off')

[f100mHz_256mV_2_500off, fig_f100mHz_256mV_2_500off] = sig_gen_input_834('key',0.1,0.256,2.5,2.56,'b');
%saveas(fig_f100mHz_256mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_256mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_256mV_2_500off.mat', 'f100mHz_256mV_2_500off')
saveas(fig_f100mHz_256mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig\f100mHz_256mV_2_500off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\f100mHz_256mV_2_500off.mat', 'f100mHz_256mV_2_500off')

%% Bandwidth Measurements
bd_data = cell(1,9);
[f100mHz_1V_2_5off, ~] = sig_gen_input_834('key',0.1,1,2.5,2.56,'b');
bd_data{1,1} = f100mHz_1V_2_5off;
for i = 1:8
    [data, fig] = sig_gen_input_834('key',i,1,2.5,2.56,'b');
    bd_data{1,i+1} = data;
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\bd_data.mat','bd_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\bd_data.mat','bd_data')

%% Dynamic Performance
f300mHz_1_5V_2_500off = sig_gen_input_834('key',0.3,1.5,2.5,2.56,'b');
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f300mHz_1_5V_2_500off.mat', 'f300mHz_1_5V_2_500off')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\f300mHz_1_5V_2_500off.mat', 'f300mHz_1_5V_2_500off')

%% CHANGE INPUT TO DIGISTANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to Digistant. Continue? [y+Enter]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data = linearity_mes(0,2.5,26,834,1,'b');
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\dig_data.mat','dig_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\dig_data.mat','dig_data')

%% CHANGE INPUT TO BATTERY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to 3V battery. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    [~,data] = serial_readout_834(5,115200,682,'b');
     pause(5);
    %Process and plot the data
    [~,~,~, bat_3V,fig_bat3V] = data_processing(data,834,2.56,1);
    %saveas(fig_bat3V,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\fig_bat3V.fig')
    %save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\bat_3V.mat', 'bat_3V')
    saveas(fig_bat3V,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig\fig_bat3V.fig')
    save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\bat_3V.mat', 'bat_3V')
end

%% CHANGE INPUT TO GND
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be grounded. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    [~,data] = serial_readout_834(5,115200,682,'b');
     pause(5);
    %Process and plot the data
    [~,~,~, in_gnd,fig_in_gnd] = data_processing(data,834,2.56,1);
    %saveas(fig_in_gnd,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\fig_in_gnd.fig')
    %save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\in_gnd.mat', 'in_gnd')
    saveas(fig_bat3V,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig\fig_bat3V.fig')
    save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data\bat_3V.mat', 'bat_3V')
end
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% -13mV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Voltage Range Measurements

[f100mHz_2_56V_2_5off_minus13, fig_f100mHz_2_56V_2_5off_minus13] = sig_gen_input_834('key',0.1,2.56,2.5-0.013,2.56,'b');
%saveas(fig_f100mHz_2_56V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_2_56V_2_5off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_2_56V_2_5off.mat', 'f100mHz_2_56V_2_5off')
saveas(fig_f100mHz_2_56V_2_5off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig2\f100mHz_2_56V_2_5off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\f100mHz_2_56V_2_5off_minus13.mat', 'f100mHz_2_56V_2_5off_minus13')

[f100mHz_1_5V_2_5off_minus13, fig_f100mHz_1_5V_2_5off_minus13] = sig_gen_input_834('key',0.1,1.5,2.5-0.013,2.56,'b');
%saveas(fig_f100mHz_1_5V_2_5off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_1_5V_2_5off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_1_5V_2_5off.mat', 'f100mHz_1_5V_2_5off')
saveas(fig_f100mHz_1_5V_2_5off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig2\f100mHz_1_5V_2_5off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\f100mHz_1_5V_2_5off_minus13.mat', 'f100mHz_1_5V_2_5off_minus13')

[f100mHz_256mV_2_500off_minus13, fig_f100mHz_256mV_2_500off_minus13] = sig_gen_input_834('key',0.1,0.256,2.5-0.013,2.56,'b');
%saveas(fig_f100mHz_256mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\f100mHz_256mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f100mHz_256mV_2_500off.mat', 'f100mHz_256mV_2_500off')
saveas(fig_f100mHz_256mV_2_500off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig2\f100mHz_256mV_2_500off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\f100mHz_256mV_2_500off_minus13.mat', 'f100mHz_256mV_2_500off_minus13')

%% Bandwidth Measurements
bd_data_minus13 = cell(1,9);
[f100mHz_1V_2_5off_minus13, ~] = sig_gen_input_834('key',0.1,1,2.5-0.013,2.56,'b');
bd_data_minus13{1,1} = f100mHz_1V_2_5off_minus13;
for i = 1:8
    [data, fig] = sig_gen_input_834('key',i,1,2.5-0.013,2.56,'b');
    bd_data_minus13{1,i+1} = data;
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\bd_data.mat','bd_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\bd_data_minus13.mat','bd_data_minus13')

%% Dynamic Performance
f300mHz_1_5V_2_500off_minus13 = sig_gen_input_834('key',0.3,1.5,2.5-0.013,2.56,'b');
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\f300mHz_1_5V_2_500off.mat', 'f300mHz_1_5V_2_500off')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\f300mHz_1_5V_2_500off_minus13.mat', 'f300mHz_1_5V_2_500off_minus13')

%% CHANGE INPUT TO DIGISTANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to Digistant. Continue? [y+Enter]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data_minus13 = linearity_mes(0,2.5,26,834,1,'b');
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\dig_data.mat','dig_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\dig_data_minus13.mat','dig_data_minus13')

%% CHANGE INPUT TO BATTERY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prompt = 'Input should be changed to 3V battery. Continue? [y+Enter]: ';
str = input(prompt,'s');
if str == 'y'
    [~,data] = serial_readout_834(5,115200,682,'b');
     pause(5);
    %Process and plot the data
    [~,~,~, bat_3V,fig_bat3V] = data_processing(data,834,2.56,1);
    %saveas(fig_bat3V,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\fig3\fig_bat3V.fig')
    %save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\room_temp\data3\bat_3V.mat', 'bat_3V')
    saveas(fig_bat3V_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\fig2\fig_bat3V_minus13.fig')
    save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_2_56V\cryo\data2\bat_3V_minus13.mat', 'bat_3V_minus13')
end


