function dec_norm = log2mat(name)

addpath aduc834/AIN1_V_in_AIN2_V_ref/Unpolar_20mV/txt_data
%data=readtable('100mHz_2mV_2.504off.txt');
data=readtable(sprintf('%s.txt',name));
data=table2array(data);
lsb = 1.192*10^(-9);
fs = 16.65;
L = 682;

table_hex=cell(1,L); %hexadecimal data cell initialization
table_bin=cell(1,L); %binary data cell initialization
table_dec=cell(1,L); %decimal data cell initialization
dec_norm=[]; %decimal normalized data array initialization

for l=1:length(table_hex)
    table_bin{l} = hexToBinaryVector(data(l),24); %conversion to binary
    table_dec{l} = binaryVectorToDecimal(table_bin{l}); %conversion to decimal
    dec_norm(l) = table_dec{l}*lsb;
end

x = (1:L)/fs; %x axis
y = dec_norm + 2.5;
figure;
plot(x,y)
xlabel('Time');
ylabel('Amplitude');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
ax.GridAlpha = 0.3;