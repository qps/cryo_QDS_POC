
function scope = Create_scope(InputBuffer, Timeout, ByteOrder)
%Create visa instrument for R&S RTM2034 Oscilloscope
    scope = visa('ni','USB0::0x0AAD::0x010F::101474::INSTR'); %USB connection : 0x0AAD manufacturer ID , 0x010F model code, 101474 serial number 
    scope.InputBufferSize = InputBuffer;
    scope.Timeout = Timeout;
    scope.ByteOrder = ByteOrder;
    
    fopen(scope);
    fprintf(scope,'*IDN?');
    idn_scope=fscanf(scope);
    fprintf('\nOscilloscope Identification string: %s\n', idn_scope)
end

