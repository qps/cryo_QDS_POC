function [table_hex,table_bin,table_dec,dec_norm,h] = data_processing(data,aduc,lsb,in_pl)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS:
%%data = raw data acquired from a serial port 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS:
%%table_hex = cell of real data in hexadecimal format
%%table_bin = cell of real data in binary format
%%table_dec = cell of real data itdecimal format
%%dec_norm  = array of decimal data normalized by 1638,based on Vref = 2.5V,
%%which is the maximum output value of the ADC.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ADuC831 or ADuC834
if aduc==831
    n1=5;
    n2=2;
    len=ceil(length(data)/6);
    lsb=610*10^(-6);
    in_pl = 1;
    fs = 38400;
    L = 1023;
elseif aduc==834
    n1=8;
    n2=5;
    len=ceil(length(data)/9-1);
    if lsb == 2.56
        lsb=0.1525*10^(-6);
    elseif lsb == 20
        lsb = 1.192*10^(-9);
    elseif lsb == 40
        lsb = 2.384*10^(-9); 
    elseif lsb == 1.28
        %lsb = 7.629*10^(-8);
        lsb = 1.5258*10^(-7);
    end
    fs = 16.65;
    %fs = 105.3;
    L = 682;
end
%% INITIALIZATION    
k=1; %data length counter initialization
table_hex=cell(1,len); %hexadecimal data cell initialization
table_bin=cell(1,len); %binary data cell initialization
table_dec=cell(1,len); %decimal data cell initialization
dec_norm=[]; %decimal normalized data array initialization

%% DATA EXTRACTION

%Data is acquired from serial port in form of ASCII characters, three of
%which are the representaion of a 12 bit hexadecimal value and they are 
%separated by \r\n ASCII characters. The first value is 's' which is pressed 
%to started the conversions so it is ignored.

for i = 2:n1:length(data)-1
    for j = 0:n2
        if (data(i+j)>=48)&&(data(i+j)<=57) %ASCII to char for numbers 0-9
            table_hex{k}(j+1) = int2str(data(i+j)-48);
            if j==n2
                k=k+1;
            end
         elseif (data(i+j)>=65)&&(data(i+j)<=70) %ASCII to char for characters A-F
            switch data(i+j)
              case 65
                table_hex{k}(j+1) = 'A';
              case 66
                table_hex{k}(j+1) = 'B';
              case 67
                table_hex{k}(j+1) = 'C';
              case 68
                table_hex{k}(j+1) = 'D';
              case 69
                table_hex{k}(j+1) = 'E';
              case 70
                table_hex{k}(j+1) = 'F';
            end
            if j==n2
                k=k+1;
            end
       elseif (data(i+j)>=97)&&(data(i+j)<=102) %ASCII to char for characters A-F
            switch data(i+j)
              case 97
                table_hex{k}(j+1) = 'a';
              case 98
                table_hex{k}(j+1) = 'b';
              case 99
                table_hex{k}(j+1) = 'c';
              case 100
                table_hex{k}(j+1) = 'd';
              case 101
                table_hex{k}(j+1) = 'e';
              case 102
                table_hex{k}(j+1) = 'f';
            end
            if j==n2
                k=k+1;
            end
        end
    end
end
   

for l=1:length(table_hex)
    table_bin{l} = hexToBinaryVector(table_hex{l},24); %conversion to binary
    table_dec{l} = binaryVectorToDecimal(table_bin{l}); %conversion to decimal
    if in_pl == 2
        if table_dec{l}>16777215/2     
            dec_norm(l) = -(16777215/2-table_dec{l})*lsb;
        elseif table_dec{l}==16777215/2
            dec_norm(l) = 0;
        elseif table_dec{l}<16777215/2
            dec_norm(l) = -(16777215/2-table_dec{l})*lsb;
        end
    elseif in_pl == 1
        dec_norm(l) = table_dec{l}*lsb;
    end
end       


%% PLOTTING

x = (1:L)/fs; %x axis
y = dec_norm;
h = figure;

plot(x,y)
xlabel('Time');
ylabel('Amplitude');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'm';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
ax.GridAlpha = 0.3;

end
  
