%{
%% Input Voltage Range Measurements

[f100mHz_20mV_2_504off, fig_f100mHz_20mV_2_504off] = sig_gen_input_834('key',0.1,0.020,2.504-0.010,20,'a');
%saveas(fig_f100mHz_20mV_2_504off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_20mV_2_504off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_20mV_2_504off.mat', 'f100mHz_20mV_2_504off')
saveas(fig_f100mHz_20mV_2_504off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig\f100mHz_20mV_2_504off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\f100mHz_20mV_2_504off.mat', 'f100mHz_20mV_2_504off')

[f100mHz_20mV_2_505off, fig_f100mHz_20mV_2_505off] = sig_gen_input_834('key',0.1,0.020,2.505-0.010,20,'a');
%saveas(fig_f100mHz_20mV_2_505off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_20mV_2_505off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_20mV_2_505off.mat', 'f100mHz_20mV_2_505off')
saveas(fig_f100mHz_20mV_2_505off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig\f100mHz_20mV_2_505off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\f100mHz_20mV_2_505off.mat', 'f100mHz_20mV_2_505off')

[f100mHz_10mV_2_500off, fig_f100mHz_10mV_2_500off] = sig_gen_input_834('key',0.1,0.010,2.5-0.005,20,'a');
%saveas(fig_f100mHz_10mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_10mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_10mV_2_500off.mat', 'f100mHz_10mV_2_500off')
saveas(fig_f100mHz_10mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig\f100mHz_10mV_2_500off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\f100mHz_10mV_2_500off.mat', 'f100mHz_10mV_2_500off')

[f100mHz_2mV_2_500off, fig_f100mHz_2mV_2_500off] = sig_gen_input_834('key',0.1,0.002,2.5-0.001,20,'a');
%saveas(fig_f100mHz_2mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_2mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_2mV_2_500off.mat', 'f100mHz_2mV_2_500off')
saveas(fig_f100mHz_2mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig\f100mHz_2mV_2_500off.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\f100mHz_2mV_2_500off.mat', 'f100mHz_2mV_2_500off')

%% Bandwidth Measurments
bd_data = cell(1,9);
bd_data{1,1} = f100mHz_10mV_2_500off;
for i = 1:8
    [data, fig] = sig_gen_input_834('key',i,0.010,2.5,20,'a');
    bd_data{1,i+1} = data;
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\bd_data.mat','bd_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\bd_data.mat','bd_data')

%% Dynamic Performance
f300mHz_10mV_2_500off = sig_gen_input_834('key',0.3,0.010,2.5,20,'a');
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f300mHz_10mV_2_500off.mat', 'f300mHz_10mV_2_500off')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\f300mHz_10mV_2_500off.mat', 'f300mHz_10mV_2_500off')

%% CHANGE INPUT TO DIGISTANT
prompt = 'Input should be changed to Digistant. Continue? [y]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data = linearity_mes(0,0.02,21,834,1,'a');
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\dig_data.mat','dig_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data\dig_data.mat','dig_data')
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% -13mV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input Voltage Range Measurements

[f100mHz_20mV_2_504off_minus13, fig_f100mHz_20mV_2_504off_minus13] = sig_gen_input_834('key',0.1,0.020,2.504-0.010-0.013,20,'a');
%saveas(fig_f100mHz_20mV_2_504off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_20mV_2_504off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_20mV_2_504off.mat', 'f100mHz_20mV_2_504off')
saveas(fig_f100mHz_20mV_2_504off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig2\f100mHz_20mV_2_504off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\f100mHz_20mV_2_504off_minus13.mat', 'f100mHz_20mV_2_504off_minus13')

[f100mHz_20mV_2_505off_minus13, fig_f100mHz_20mV_2_505off_minus13] = sig_gen_input_834('key',0.1,0.020,2.505-0.010-0.013,20,'a');
%saveas(fig_f100mHz_20mV_2_505off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_20mV_2_505off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_20mV_2_505off.mat', 'f100mHz_20mV_2_505off')
saveas(fig_f100mHz_20mV_2_505off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig2\f100mHz_20mV_2_505off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\f100mHz_20mV_2_505off_minus13.mat', 'f100mHz_20mV_2_505off_minus13')

[f100mHz_10mV_2_500off_minus13, fig_f100mHz_10mV_2_500off_minus13] = sig_gen_input_834('key',0.1,0.010,2.5-0.005-0.013,20,'a');
%saveas(fig_f100mHz_10mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_10mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_10mV_2_500off.mat', 'f100mHz_10mV_2_500off')
saveas(fig_f100mHz_10mV_2_500off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig2\f100mHz_10mV_2_500off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\f100mHz_10mV_2_500off_minus13.mat', 'f100mHz_10mV_2_500off_minus13')

[f100mHz_2mV_2_500off_minus13, fig_f100mHz_2mV_2_500off_minus13] = sig_gen_input_834('key',0.1,0.002,2.5-0.001-0.013,20,'a');
%saveas(fig_f100mHz_2mV_2_500off,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\fig3\f100mHz_2mV_2_500off.fig')
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f100mHz_2mV_2_500off.mat', 'f100mHz_2mV_2_500off')
saveas(fig_f100mHz_2mV_2_500off_minus13,'aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\fig2\f100mHz_2mV_2_500off_minus13.fig')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\f100mHz_2mV_2_500off_minus13.mat', 'f100mHz_2mV_2_500off_minus13')

%% Bandwidth Measurments
bd_data = cell(1,9);
bd_data{1,1} = f100mHz_10mV_2_500off_minus13;
for i = 1:8
    [data, fig] = sig_gen_input_834('key',i,0.010,2.5-0.013,20,'a');
    bd_data{1,i+1} = data;
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\bd_data.mat','bd_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\bd_data_minus13.mat','bd_data')

%% Dynamic Performance
f300mHz_10mV_2_500off_minus13 = sig_gen_input_834('key',0.3,0.010,2.5-0.013,20,'a');
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\f300mHz_10mV_2_500off.mat', 'f300mHz_10mV_2_500off')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\f300mHz_10mV_2_500off_minus13.mat', 'f300mHz_10mV_2_500off_minus13')

%% CHANGE INPUT TO DIGISTANT
prompt = 'Input should be changed to Digistant. Continue? [y]: ';
str = input(prompt,'s');

%% Linearity Measurements
if str == 'y'
    dig_data_minus13 = linearity_mes(0,0.02,21,834,1,'a');
end
%save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\room_temp\data3\dig_data.mat','dig_data')
save('aduc834\AIN1_V_in_AIN2_V_ref\Unipolar_20mV\cryo\data2\dig_data_minus13.mat','dig_data_minus13')



