

%Create&Open generator with Inputs: timeout, comport
gen = Create_gen(10,6);  
%%Function Generator General Output Configurations
fprintf(gen,'FUNC SIN'); %Select the output function
fprintf(gen,'OUTP:LOAD 10000'); %Define the instruments output load as 50ohm
fprintf(gen,'VOLT VOLT'); %Set the Amplitude Unit
fprintf(gen,'VOLT 1'); %Select the Amplitude Voltage Value
fprintf(gen,'OUTP:OFFS 1'); %Activate/Disactivate the output offset voltage
fprintf(gen,'VOLT:OFFS 0.5'); %Define the output offset value depending on

%% SWEEP MODE

fprintf(gen,'FREQ:STAR 100'); %Define the start frequency of the sweep
fprintf(gen,'FREQ:STOP 15000'); %Define the stop frequency of the sweep
fprintf(gen,'SWE:SPAC LIN'); %Define the sweep spacing {Linear, Logarithmic}
fprintf(gen,'SWE:TIME 0.045'); %Define the sweep time (sec)
fprintf(gen,'MARK:FREQ 100'); %Define the sweep marker frequency

fprintf(gen,'SWE:STAT 1'); %Activate the sweep function
[~,data_sweep] = serial_readout(0,4,9600,1000); %Acquire data from uC

[~,~,~,dec_norm] = data_processing(data_sweep);


%{
y = 20*log(fft_data);
x = linspace(100,5000,1000);
fprintf('%d , %d',size(x,2),size(y,1))
figure;
plot(x',y,'Color','y')
xlabel('Frequency (Hz)');
ylabel('Amplitude (db)');
grid on 
grid minor
ax = gca;
ax.Color = 'k';
ax.LineWidth = 1.5;
ax.GridColor = 'w';
ax.MinorGridColor = 'g';
ax.MinorGridLineStyle = '-.';
ax.GridLineStyle = '-';
%}

fprintf(gen,'SWE:STAT 0');
fclose(gen);





















