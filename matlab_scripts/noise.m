function [pd, h] = noise(data)


figure;
h = histfit(data/(1.192*10^(-9)));
%h = histfit(data/(0.1525*10^(-6)));
%h = histogram(data/(610*10^(-6)));
%h = histogram(data/(0.1525*10^(-6)));
xlabel('LSB')
ylabel('count')

pd = fitdist(data','Normal');



end
