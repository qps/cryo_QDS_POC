/*
 * ADuC831.C
 *
 *  Created on: Sep 29, 2017
 *      Author: tpridii
 */

#include<stdio.h>
#include <\ADI\ADuC831.h>

sbit LED  = 0x0B5;
sbit PWM0 = 0x0B3;
sbit PWM1 = 0x0B4;
//unsigned char xdata adc_datah[1000][8],adc_datal[1000][8];
unsigned int result;




typedef struct data_struct {
	unsigned char  H;
	unsigned char  L;
} data_struct;
xdata data_struct adc[1000];

/*
data_struct adc_data;

data_struct get_data ()
{
	data_struct adc_data;

	for(i=0;i<20;i++)
			{
			ADCCON2 = 0x10;
			while(!ADCI){};
			adc_data.H[i] = ADCDATAH;
			adc_data.L[i] = ADCDATAL;
			//printf("%2X%2X\n", ADCDATAH, ADCDATAL);
			PWM0L  = ADCDATAL;
			PWM0H  = ADCDATAH;
			ADCI = 0;
			LED^=1;
			}

	return adc_data;
}
*/


void main (void)
{

int i, c1, correct, start;
char c2, chan;
/*UART CONFIGURATION*/
T3CON = 0x085;
T3FD = 0x08;
SCON = 0x052;


CFG831 = 0xC1; //extended SP enabled, PWM output pins selected as P3.4 and P3.3, XRAM enabled

PWMCON = 0x5F; //dual 8 bit PWM - mode 5
ADCCON1 = 0x08C; // power up ADC /32 + 4 acq clock
EA = 1; //enable interrupts
EADC = 1; //enable ADC interrupts
ES = 1; //enable UART interrupts

for(;;)
{
	while(!correct)
		{
		printf("Press 0 for Channel_0 or 1 for Channel_1\n");
		c1=getchar();
		if(c1=='0') {chan=0x10;correct=1;}
		else if (c1=='1') {chan=0x11;correct=1;}
		else {printf("Error\n"); correct=0;}
		}
	while(!start)
		{
		printf("Press 's' to start conversions\n");
		c2 = getchar();
		if(c2=='s')
			{
			for(i=0;i<1000;i++)
				{
				ADCCON2 = chan; //Channel 0, Single Conversion
				while(!ADCI){};
				/*Storing data into struct of 1000 length*/
				adc[i].H = ADCDATAH;
				adc[i].L = ADCDATAL;

				/*PWM CONFIGURATION*/
				PWM0L  = ADCDATAH; // P3.4 duty cycle
				PWM0H  = ADCDATAH; // P3.3 duty cycle
				PWM1L  = ADCDATAL; // P3.4 output resolution
				PWM1H  = ADCDATAL; // P3.3 output resolution
				LED^=1; //Blink LED
				ADCI = 0;
				}
			start = 1;
			}
		else {start = 0;}
		}

	for(i=0;i<1000;i++)
			{
			result = (unsigned int)(adc[i].H & 0x0F); //Extracting the High bits
			result = (result<<8)+(unsigned int)(adc[i].L); //12-bit adc data output
			printf("%03X\n",result);
			}

	correct = 0;
	start = 0;
}
}
