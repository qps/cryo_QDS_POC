
#line 1 ".\src/ADuC831.c" /0





 
 
  
#line 1 "C:\KEIL\V801\INC\STDIO.H" /0






 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 typedef unsigned int size_t;
 
 
 #pragma SAVE
 #pragma REGPARMS
 extern char _getkey (void);
 extern char getchar (void);
 extern char ungetchar (char);
 extern char putchar (char);
 extern int printf   (const char *, ...);
 extern int sprintf  (char *, const char *, ...);
 extern int vprintf  (const char *, char *);
 extern int vsprintf (char *, const char *, char *);
 extern char *gets (char *, int n);
 extern int scanf (const char *, ...);
 extern int sscanf (char *, const char *, ...);
 extern int puts (const char *);
 
 #pragma RESTORE
 
 
 
#line 8 ".\src/ADuC831.c" /0
 
  
#line 1 "C:\KEIL\V801\INC\ADI\ADUC831.H" /0








 
 
 
 sfr   P0        = 0x80;
 sfr   SP        = 0x81;
 sfr   DPL       = 0x82;
 sfr   DPH       = 0x83;
 sfr   DPP       = 0x84;
 sfr   PCON      = 0x87;
 sfr   TCON      = 0x88;
 sfr   TMOD      = 0x89;
 sfr   TL0       = 0x8A;
 sfr   TL1       = 0x8B;
 sfr   TH0       = 0x8C;
 sfr   TH1       = 0x8D;
 sfr   P1        = 0x90;
 sfr   SCON      = 0x98;
 sfr   SBUF      = 0x99;
 sfr   I2CDAT    = 0x9A;
 sfr   I2CADD    = 0x9B;
 sfr   T3FD      = 0x9D;
 sfr   T3CON     = 0x9E;
 sfr   P2        = 0xA0;
 sfr   TIMECON   = 0xA1;
 sfr   HTHSEC    = 0xA2;
 sfr   SEC       = 0xA3;
 sfr   MIN       = 0xA4;
 sfr   HOUR      = 0xA5;
 sfr   INTVAL    = 0xA6;
 sfr   DPCON     = 0xA7;
 sfr   IE        = 0xA8;
 sfr   IEIP2     = 0xA9;
 sfr   PWMCON    = 0xAE;
 sfr   CFG831    = 0xAF;
 sfr   P3        = 0xB0;
 sfr   PWM0L     = 0xB1;
 sfr   PWM0H     = 0xB2;
 sfr   PWM1L     = 0xB3;
 sfr   PWM1H     = 0xB4;
 sfr   SPH       = 0xB7;
 sfr   IP        = 0xB8;
 sfr   ECON      = 0xB9;
 sfr   EDATA1    = 0xBC;
 sfr   EDATA2    = 0xBD;
 sfr   EDATA3    = 0xBE;
 sfr   EDATA4    = 0xBF;
 sfr   WDCON     = 0xC0;
 sfr   CHIPID    = 0xC2;
 sfr   EADRL     = 0xC6;
 sfr   EADRH     = 0xC7;
 sfr   T2CON     = 0xC8;
 sfr   RCAP2L    = 0xCA;
 sfr   RCAP2H    = 0xCB;
 sfr   TL2       = 0xCC;
 sfr   TH2       = 0xCD;
 sfr   PSW       = 0xD0;
 sfr   DMAL      = 0xD2;
 sfr   DMAH      = 0xD3;
 sfr   DMAP      = 0xD4;
 sfr   ADCCON2   = 0xD8;
 sfr   ADCDATAL  = 0xD9;
 sfr   ADCDATAH  = 0xDA;
 sfr   PSMCON    = 0xDF;
 sfr   ACC       = 0xE0;
 sfr   DCON      = 0xE8;
 sfr   I2CCON    = 0xE8;
 sfr   ADCCON1   = 0xEF;
 sfr   B         = 0xF0;
 sfr   ADCOFSL   = 0xF1;
 sfr   ADCOFSH   = 0xF2;
 sfr   ADCGAINL  = 0xF3;
 sfr   ADCGAINH  = 0xF4;
 sfr   ADCCON3   = 0xF5;
 sfr   SPIDAT    = 0xF7;
 sfr   SPICON    = 0xF8;
 sfr   DAC0L     = 0xF9;
 sfr   DAC0H     = 0xFA;
 sfr   DAC1L     = 0xFB;
 sfr   DAC1H     = 0xFC;
 sfr   DACCON    = 0xFD;
 
 
 
 sbit  TF1       = 0x8F;
 sbit  TR1       = 0x8E;
 sbit  TF0       = 0x8D;
 sbit  TR0       = 0x8C;
 sbit  IE1       = 0x8B;
 sbit  IT1       = 0x8A;
 sbit  IE0       = 0x89;
 sbit  IT0       = 0x88;
 
 sbit  T2EX      = 0x91;
 sbit  T2        = 0x90;
 
 sbit  SM0       = 0x9F;
 sbit  SM1       = 0x9E;
 sbit  SM2       = 0x9D;
 sbit  REN       = 0x9C;
 sbit  TB8       = 0x9B;
 sbit  RB8       = 0x9A;
 sbit  TI        = 0x99;
 sbit  RI        = 0x98;
 
 sbit  EA        = 0xAF;
 sbit  EADC      = 0xAE;
 sbit  ET2       = 0xAD;
 sbit  ES        = 0xAC;
 sbit  ET1       = 0xAB;
 sbit  EX1       = 0xAA;
 sbit  ET0       = 0xA9;
 sbit  EX0       = 0xA8;
 
 sbit  RD        = 0xB7;
 sbit  WR        = 0xB6;
 sbit  T1        = 0xB5;
 sbit  T0        = 0xB4;
 sbit  INT1      = 0xB3;
 sbit  INT0      = 0xB2;
 sbit  TXD       = 0xB1;
 sbit  RXD       = 0xB0;
 
 sbit  PSI       = 0xBF;
 sbit  PADC      = 0xBE;
 sbit  PT2       = 0xBD;
 sbit  PS        = 0xBC;
 sbit  PT1       = 0xBB;
 sbit  PX1       = 0xBA;
 sbit  PT0       = 0xB9;
 sbit  PX0       = 0xB8;
 
 sbit  PRE3      = 0xC7;
 sbit  PRE2      = 0xC6;
 sbit  PRE1      = 0xC5;
 sbit  PRE0      = 0xC4;
 sbit  WDIR      = 0xC3;
 sbit  WDS       = 0xC2;
 sbit  WDE       = 0xC1;
 sbit  WDWR      = 0xC0;
 
 sbit  TF2       = 0xCF;
 sbit  EXF2      = 0xCE;
 sbit  RCLK      = 0xCD;
 sbit  TCLK      = 0xCC;
 sbit  EXEN2     = 0xCB;
 sbit  TR2       = 0xCA;
 sbit  CNT2      = 0xC9;
 sbit  CAP2      = 0xC8;
 
 sbit  CY        = 0xD7;
 sbit  AC        = 0xD6;
 sbit  F0        = 0xD5;
 sbit  RS1       = 0xD4;
 sbit  RS0       = 0xD3;
 sbit  OV        = 0xD2;
 sbit  F1        = 0xD1;
 sbit  P         = 0xD0;
 
 sbit  ADCI      = 0xDF;
 sbit  DMA       = 0xDE;
 sbit  CCONV     = 0xDD;
 sbit  SCONV     = 0xDC;
 sbit  CS3       = 0xDB;
 sbit  CS2       = 0xDA;
 sbit  CS1       = 0xD9;
 sbit  CS0       = 0xD8;
 
 sbit  MDO       = I2CCON^7;
 sbit  MDE       = I2CCON^6;
 sbit  MCO       = I2CCON^5;
 sbit  MDI       = I2CCON^4;
 sbit  I2CM      = I2CCON^3;
 sbit  I2CRS     = I2CCON^2;
 sbit  I2CTX     = I2CCON^1;
 sbit  I2CI      = I2CCON^0;
 
 sbit  D1        = DCON^7;
 sbit  D1EN      = DCON^6;
 sbit  D0        = DCON^5;
 sbit  D0EN      = DCON^3;
 
 sbit  ISPI      = 0xFF;
 sbit  WCOL      = 0xFE;
 sbit  SPE       = 0xFD;
 sbit  SPIM      = 0xFC;
 sbit  CPOL      = 0xFB;
 sbit  CPHA      = 0xFA;
 sbit  SPR1      = 0xF9;
 sbit  SPR0      = 0xF8;
#line 9 ".\src/ADuC831.c" /0
 
 
 sbit LED  = 0x0B5;
 sbit PWM0 = 0x0B3;
 sbit PWM1 = 0x0B4;
 
 unsigned int result;
 
 
 
 
 typedef struct data_struct {
 unsigned char  H;
 unsigned char  L;
 } data_struct;
 xdata data_struct adc[1000];
 






















 
 
 
 void main (void)
 {
 
 int i, c1, correct, start;
 char c2, chan;
 
 T3CON = 0x085;
 T3FD = 0x08;
 SCON = 0x052;
 
 
 CFG831 = 0xC1;  
 
 PWMCON = 0x5F;  
 ADCCON1 = 0x08C;  
 EA = 1;  
 EADC = 1;  
 ES = 1;  
 
 for(;;)
 {
 while(!correct)
 {
 printf("Press 0 for Channel_0 or 1 for Channel_1\n");
 c1=getchar();
 if(c1=='0') {chan=0x10;correct=1;}
 else if (c1=='1') {chan=0x11;correct=1;}
 else {printf("Error\n"); correct=0;}
 }
 while(!start)
 {
 printf("Press 's' to start conversions\n");
 c2 = getchar();
 if(c2=='s')
 {
 for(i=0;i<1000;i++)
 {
 ADCCON2 = chan;  
 while(!ADCI){};
 
 adc[i].H = ADCDATAH;
 adc[i].L = ADCDATAL;
 
 
 PWM0L  = ADCDATAH;  
 PWM0H  = ADCDATAH;  
 PWM1L  = ADCDATAL;  
 PWM1H  = ADCDATAL;  
 LED^=1;  
 ADCI = 0;
 }
 start = 1;
 }
 else {start = 0;}
 }
 
 for(i=0;i<1000;i++)
 {
 result = (unsigned int)(adc[i].H & 0x0F);  
 result = (result<<8)+(unsigned int)(adc[i].L);  
 printf("%03X\n",result);
 }
 
 correct = 0;
 start = 0;
 }
 }
